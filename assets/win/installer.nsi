!define APP_NAME "Mewpot_Plugin_Installer"

!include MUI.nsh
!define MUI_PAGE_HEADER_TEXT "${APP_NAME}"
!define MUI_ICON "D:\1.pj\mewpot-ppropanel-installer\assets\win\icon.ico"

!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_LANGUAGE "Korean"

OutFile "D:\1.pj\mewpot-ppropanel-installer\release\win\${APP_NAME} Setup.exe"

ShowInstDetails "nevershow"

Section
# Check files
Push "$TEMP\${APP_NAME}"
Call isEmptyDir
Pop $0
SetOutPath "$TEMP\${APP_NAME}"
File /r "D:\1.pj\mewpot-ppropanel-installer\release\win\Mewpot_Plugin_Installer-win32\*"

exec '"$TEMP\${APP_NAME}\${APP_NAME}.exe"'
Quit
SectionEnd
