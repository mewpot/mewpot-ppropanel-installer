global.Messages = function() {

  // Error messages are a best guess interpretation of the messages provided here: http://www.adobeexchange.com/resources/19#errors
  var errorMessages = [
    {
      codes: [0],
      message: 'Installation failed because it could not be downloaded. It should not be possible to receive this error.'
    },{
      codes: [152,154,155,156,157,158,160,161,162,163,164,165,168,169,171,172,176,178,179],
      message: 'Installation failed because of a file operation error.'
    },{
      codes: [251,252,253,254,255,256,257,259,260,261,265,266,267,268,269,270,271,272],
      message: 'Installation failed because ZXPInstaller could not parse the .zxp file.'
    },{
      codes: [500,501,502,503,504,505,506,507,508,508,509],
      message: 'Installation failed because ZXPInstaller could not update the database. It should not be possible to receive this error.'
    },{
      codes: [601,602,603,604,651,652,653],
      message: 'Installation failed because it the ZXPInstaller could not check the license online.'
    },{
      codes: [159],
      message: 'ZXPInstaller cannot install this type of file. Please use a .zxp file.'
    },{
      codes: [175],
      message: 'You must run ZXPInstaller in administrator mode to install extensions.'
    },{
      codes: [201],
      message: 'Installation failed because the extension invalid.'
    },{
      codes: [402],
      message: 'Installation failed because the extension does not contain a valid code signature.'
    },{
      codes: [403,411],
      message: '플러그인과 설치된 프리미어 프로가 호환되지 않아 설치에 실패함'
    },{
      codes: [407,408],
      message: 'Installation failed because this extension requires another extension.'
    },{
      codes: [412],
      message: '이미 플러그인이 설치되어 있습니다.'
    },{
      codes: [418],
      message: 'Installation failed because a newer version of the extension is installed.'
    },{
      codes: [456],
      message: 'Please close all Adobe applications before installing extensions.'
    },{
      codes: [458],
      message: '프리미어 프로가 설치되어 있지 않아 플러그인 설치에 실패함'
    },{
      codes: [459],
      message: '플러그인과 설치된 프리미어 프로가 호환되지 않아 설치에 실패함'
    }
  ];

  this.errors = {
    get: function(code) {
      var msg = '';
      $.each(errorMessages, function(key, error){
        if ($.inArray(code, error.codes) > -1) msg = error.message;
      });
      return (msg !== '') ? msg : null;
    }
  };

  this.ui = {
    dragToInstall: 'Drag a ZXP file or click here to select a file.',
    dropToInstall: 'Drop your file here to install it.',
    installing: '플러그인 설치중…',
    installed: '플러그인 설치가 완료되었습니다. 프리미어 프로를 재시작하세요'
  };

  return this;
};
