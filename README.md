# Mewpot PProPanel Installer

이 프로젝트는 ZXPInstaller를 포크하여 뮤팟 PProPanel을 설치하는 프로그램으로 변경시킨 프로젝트입니다. GPL 라이선스에 따라 소스코드가 공개되어 있습니다.

# DMG, EXE 만들기
시도를 하다하다 실패해서 결국 DMG파일은 Mac OS에서, EXE는 윈도우에서 생성했다
우선 DMG 파일 생성하는 방법
```
npm install
npm run pack:osx
```
이러면 app 파일이 release/osx 폴더에 생긴다. 이제 이걸 Disk Utility를 이용해서 DMG Image를 만들면 끝!

EXE파일은 더 복잡하다.
```
npm run build:win
```
이후 [nsis](https://nsis.sourceforge.io/Download)를 설치해야 한다. 그리고 EXE 파일을 이 프로그램을 이용해 만든다
```
makensis assets/win/installer.nsi
```
파일명이 매칭이 안되면 제대로 안만들어진다. nsi파일을 수정해라

# How it works

ZXPInstaller uses Electron (http://electron.atom.io) to create a cross platform HTML/node.js app. The app spawns a child process for the Adobe Extension Manager command line utility to install the ZXP.

# Credits

ZXPInstaller was originally created by [**@maxoos**](https://github.com/maxoos).
